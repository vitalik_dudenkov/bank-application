CREATE DATABASE soft0064_bank_db;
	
CREATE TABLE clients(
	id bigint PRIMARY KEY AUTO_INCREMENT,
	full_company_name character varying(255) not null,
	short_company_name character varying(30) not null,
	company_address character varying(500) not null,
	company_legal_form BIGINT not null
);

CREATE TABLE banks(
	BIC character varying(20) PRIMARY KEY,
	title character varying(255) not null
);

	
CREATE TABLE deposits(
	id bigint PRIMARY KEY AUTO_INCREMENT,
	client_id bigint not null REFERENCES clients(id),
	bank_bic character varying(20) not null REFERENCES banks(BIC),
	opening_date DATE not null,
	interest_rate numeric(4, 2) not null,
	term_in_months smallint not null
)


INSERT INTO clients(full_company_name, short_company_name, company_address, company_legal_form)
VALUES ('Horns And Hoofs', 'H A H', 'Perm city, 75, KIM street', 3);

INSERT INTO banks VALUES
('044525593', 'Alpha-Bank'),
('044525225', 'Sberbank');

INSERT INTO deposits(client_id, bank_bic, opening_date, interest_rate, term_in_months)
VALUES
(1, '044525593', '2017-02-20', 14.75, 48),
(1, '044525225', '2020-01-30', 8.75, 20);
