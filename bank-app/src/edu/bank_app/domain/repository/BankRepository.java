package edu.bank_app.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import edu.bank_app.domain.Bank;

public interface BankRepository extends CrudRepository<Bank, String> {

	// ������ ����������
	@Query(value = "SELECT b FROM Bank b WHERE b.title LIKE '%' || :keyword || '%'")
	public List<Bank> search(@Param("keyword") String keyword);
}
