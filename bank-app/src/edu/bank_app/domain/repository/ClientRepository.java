package edu.bank_app.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.bank_app.domain.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {
	
	// ����� ��� ���������� �� ����� ��������
	@Query(value = "SELECT c FROM Client c ORDER BY c.fullCompanyName")
	public List<Client> findAllWithSorting();

}
