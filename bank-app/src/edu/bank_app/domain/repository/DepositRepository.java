package edu.bank_app.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.bank_app.domain.Bank;
import edu.bank_app.domain.Client;
import edu.bank_app.domain.Deposit;

public interface DepositRepository extends CrudRepository<Deposit, Long>{
	
	List<Deposit> findAllByClient(Client client);
	
	List<Deposit> findAllByBank(Bank bank);
}
