package edu.bank_app.addons;

public enum LegalForm {
	SP,
	FARM,
	JSC,
	LLC,
	PJSC,
	NJSC,
	GP,
	LP,
	EP;
}
