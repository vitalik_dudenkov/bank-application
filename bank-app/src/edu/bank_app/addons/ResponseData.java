package edu.bank_app.addons;


/**
 * ����� <code>ResponseData</code> �������� �������� ���������� ������� � ��������� � ��������� �����������.
 * ������������ �� ���� �������� � �������� ������������� ��������
 * 
 * @author DudenkovVI
 *
 */
public class ResponseData {
	private int errorCode;
	private String data;
	
	public ResponseData() {
		super();
	}

	public ResponseData(int errorCode, String data) {
		super();
		this.errorCode = errorCode;
		this.data = data;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	
	
}
