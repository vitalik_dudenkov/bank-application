package edu.bank_app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.bank_app.addons.ResponseData;
import edu.bank_app.service.BankService;
import edu.bank_app.service.BankServiceImpl;

/**
 * ����� <code>BankController</code> �������� �� �������, ��������� � ��������� Bank
 * 
 * @author DudenkovVI
 *
 */

@RestController
@RequestMapping("/bank")
public class BankController {

	@Autowired
	private BankService service;
	
	@GetMapping("/allBanks")
	public ResponseEntity<String> allBanks(){
		ResponseData data = service.listAll();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);			
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/editBank")
	public ResponseEntity<String> editBank(@RequestBody String bank){
		ResponseData data = service.editBank(bank);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/removeBank")
	public ResponseEntity<String> removeBank(@RequestBody String bank){
		ResponseData data = service.removeBank(bank);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/addBank")
	public ResponseEntity<String> addBank(@RequestBody String bank){
		ResponseData data = service.addBank(bank);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(value = "/search")
	public ResponseEntity<String> search(@RequestParam(value = "keyWord") String keyWord) {
		ResponseData data = service.searchBanks(keyWord);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
}
