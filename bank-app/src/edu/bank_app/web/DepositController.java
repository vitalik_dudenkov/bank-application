package edu.bank_app.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.bank_app.addons.ResponseData;
import edu.bank_app.service.DepositService;
import edu.bank_app.service.DepositServiceImpl;

/**
 * ����� <code>DepositController</code> �������� �� �������, ��������� � ��������� Deposit
 * 
 * @author DudenkovVI
 *
 */

@RestController
@RequestMapping("/deposit")
public class DepositController {

	@Autowired
	private DepositService service;
	
	@GetMapping("/allDeposits")
	public ResponseEntity<String> allDeposits(){
		ResponseData data = service.listAll();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/editDeposit")
	public ResponseEntity<String> editDeposit(@RequestBody String deposit){
		ResponseData data = service.editDeposit(deposit);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/createDeposit")
	public ResponseEntity<String> createDeposit(@RequestBody String deposit){
		ResponseData data = service.createDeposit(deposit);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/removeDeposit")
	public ResponseEntity<String> removeDeposit(@RequestBody String deposit){
		ResponseData data = service.removeDeposit(deposit);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("/getDepositsOfClient")
	public ResponseEntity<String> getDepositsOfClient(@RequestParam("clientId") String clientId){
		ResponseData data;
		try {
			data = service.getDepositsOfClient(Long.parseLong(clientId));
		}catch (Exception e) {
			data = new ResponseData(-1, "���������� �������� �� �����!");
		}
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("/getDepositsOfBank")
	public ResponseEntity<String> getDepositsOfBank(@RequestParam("bankBic") String bankBic){
		ResponseData data = service.getDepositsOfBank(bankBic);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
}
