package edu.bank_app.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.bank_app.addons.ResponseData;
import edu.bank_app.service.ClientService;
import edu.bank_app.service.ClientServiceImpl;

/**
 * ����� <code>ClientController</code> �������� �� �������, ��������� � ��������� Client
 * 
 * @author DudenkovVI
 *
 */

@RestController
@RequestMapping("/client")
public class ClientController {
	
	@Autowired
	private ClientService service;
	
	@GetMapping("/allClients")
	public ResponseEntity<String> allClients(){
		ResponseData data = service.listAll();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/editClient")
	public ResponseEntity<String> editClient(@RequestBody String client){
		ResponseData data = service.editClient(client);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);			
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/removeClient")
	public ResponseEntity<String> removeClient(@RequestBody String client){
		ResponseData data = service.removeClient(client);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);		
//		return new ResponseEntity<String>(data.getData(), data.getErrorCode() > 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);

	}
	
	@PostMapping("/addClient")
	public ResponseEntity<String> addClient(@RequestBody String client){
		ResponseData data = service.addClient(client);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);				
	}
	
	@GetMapping("/sortedClients")
	public ResponseEntity<String> sortedClients(){
		ResponseData data = service.allSortedClients();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);						
	}
	
}
