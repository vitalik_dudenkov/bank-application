package edu.bank_app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.bank_app.addons.ResponseData;
import edu.bank_app.domain.Bank;
import edu.bank_app.domain.repository.BankRepository;

@Service("BankService")
@Transactional
public class BankServiceImpl implements BankService {
	
	@Autowired
	private BankRepository repository;
	
	public ResponseData listAll(){
		JSONObject obj = new JSONObject();
		JSONArray banks = new JSONArray();
		try {
			Iterable<Bank> data = repository.findAll();
			for(Bank b : data) {
				banks.put(new JSONObject()
							.put("bic", b.getBic())
							.put("title", b.getTitle()));
			}
			obj.put("banks", banks);
			return new ResponseData(1, obj.toString());			
		}catch (Exception e) {
			return new ResponseData(-1, "�� ����� ���������� ������ �������� ������! ���������� � ��������������!");
		}

	}
	
	public ResponseData editBank(String data) {
		try {
			JSONObject bankObj = new JSONObject(data);
			Bank bank = repository.findById(bankObj.getString("bic")).get();
			bank.setTitle(bankObj.getString("title"));
			repository.save(bank);
			return new ResponseData(1, "Entity successfully saved");
		}catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������������� �������� ����������� ��������!");
		}
	}
	
	public ResponseData removeBank(String data) {
		try {
			JSONObject bankObj = new JSONObject(data);
			Bank bank = repository.findById(bankObj.getString("bic")).get();
			repository.delete(bank);
			return new ResponseData(1, "Entity successfully removed");
		}catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������� �������� ����������� ��������!");
		}
	}
	
	public ResponseData addBank(String data) {
		try {
			JSONObject bankObj = new JSONObject(data);
			Bank bank = new Bank();
			bank.setBic(bankObj.getString("bic"));
			bank.setTitle(bankObj.getString("title"));
			
			repository.save(bank);
			return new ResponseData(1, "Entity successfully created");
		}catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������� �������� ����������� ��������!");
		}

	}
	
	public ResponseData searchBanks(String keyWord) {
		JSONObject resultObj = new JSONObject();
		JSONArray banksArr = new JSONArray();
		
		try {
			List<Bank> banks = repository.search(keyWord);
			
			for(Bank b : banks) {
				banksArr.put(new JSONObject()
							.put("bic", b.getBic())
							.put("title", b.getTitle()));
			}
			resultObj.put("banks", banks);
			return new ResponseData(1, resultObj.toString());
		}catch (Exception e) {
			return new ResponseData(-1, "�� ����� ���������� ������ �������� ������! ���������� � ��������������!");
		}
	}

}
