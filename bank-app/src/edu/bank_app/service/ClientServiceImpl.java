package edu.bank_app.service;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import edu.bank_app.addons.LegalForm;
import edu.bank_app.addons.ResponseData;
import edu.bank_app.domain.Client;
import edu.bank_app.domain.repository.ClientRepository;

@Service("ClientService")
@Transactional
public class ClientServiceImpl implements ClientService {
	
	@Autowired
	private ClientRepository repository;
	
	public ResponseData listAll(){		
		JSONObject obj = new JSONObject();
		JSONArray clients = new JSONArray();
		try {
			Iterable<Client> data = repository.findAll();
			for(Client c : data) {
				clients.put(new JSONObject()
							.put("companyFullName", c.getFullCompanyName())
							.put("companyShortName", c.getShortCompanyName())
							.put("companyAddress", c.getCompanyAddress())
							.put("companyLegalForm", c.getCompanyLegalForm()));
			}
			obj.put("clients", clients);
			return new ResponseData(1, obj.toString());
		} catch (Exception e) {
			return new ResponseData(-1, "�� ����� ���������� ������ �������� ������! ���������� � ��������������!");
		}

	}
	
	public ResponseData editClient(String data) {
		try {
			JSONObject clientObj = new JSONObject(data);
			
			Client client = repository.findById(clientObj.getLong("id")).get();
			client.setFullCompanyName(clientObj.getString("companyFullName"));
			client.setShortCompanyName(clientObj.getString("companyShortName"));
			client.setCompanyAddress(clientObj.getString("companyAddress"));
			client.setCompanyLegalForm(clientObj.getString("companyLegalForm"));
			
			repository.save(client);
			return new ResponseData(1, "Entity successfully saved");			
		} catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������������� �������� ����������� ��������!");
		}

	}

	public ResponseData removeClient(String data) {
		try {
			JSONObject clientObj = new JSONObject(data);
			Client client = repository.findById(clientObj.getLong("id")).get();
			repository.delete(client);
			return new ResponseData(1, "Entity successfully removed");			
		}catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������� �������� ����������� ��������!");
		}

	}
	
	public ResponseData addClient(String data) {
		try {
			JSONObject clientObj = new JSONObject(data);
			
			Client client = new Client();
			client.setFullCompanyName(clientObj.getString("companyFullName"));
			client.setShortCompanyName(clientObj.getString("companyShortName"));
			client.setCompanyAddress(clientObj.getString("companyAddress"));
			client.setCompanyLegalForm(clientObj.getString("companyLegalForm"));
			
			repository.save(client);
			return new ResponseData(1, "Entity successfully created");			
		} catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������� �������� ����������� ��������!");
		}
	}
	
	public ResponseData allSortedClients() {
		JSONObject obj = new JSONObject();
		JSONArray clients = new JSONArray();
		
		try {
			List<Client> data = repository.findAllWithSorting();
			for(Client c : data) {
				clients.put(new JSONObject()
							.put("companyFullName", c.getFullCompanyName())
							.put("companyShortName", c.getShortCompanyName())
							.put("companyAddress", c.getCompanyAddress())
							.put("companyLegalForm", c.getCompanyLegalForm()));
			}
			obj.put("clients", clients);
			return new ResponseData(1, obj.toString());			
		}catch (Exception e) {
			return new ResponseData(-1, "�� ����� ���������� ������ �������� ������! ���������� � ��������������!");
		}
	}
}
