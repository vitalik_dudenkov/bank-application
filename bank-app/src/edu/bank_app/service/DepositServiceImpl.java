package edu.bank_app.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.bank_app.addons.ResponseData;
import edu.bank_app.domain.Bank;
import edu.bank_app.domain.Client;
import edu.bank_app.domain.Deposit;
import edu.bank_app.domain.repository.BankRepository;
import edu.bank_app.domain.repository.ClientRepository;
import edu.bank_app.domain.repository.DepositRepository;

@Service("DepositService")
@Transactional
public class DepositServiceImpl implements DepositService {

	@Autowired
	private DepositRepository repository;
	
	@Autowired
	private BankRepository bankRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	public ResponseData listAll(){
		JSONObject obj = new JSONObject();
		JSONArray deposits = new JSONArray();
		try {
			Iterable<Deposit> data = repository.findAll();
			for(Deposit d : data) {
				deposits.put(new JSONObject()
							.put("companyFullName", d.getClient().getFullCompanyName())
							.put("bankTitle", d.getBank().getTitle())
							.put("openingDate", d.getOpeningDate())
							.put("interestRate", d.getInterestRate())
							.put("termInMonths", d.getTermInMonths()));
			}
			obj.put("deposits", deposits);
			return new ResponseData(1, obj.toString());
		}catch (Exception e) {
			return new ResponseData(-1, "�� ����� ���������� ������ �������� ������! ���������� � ��������������!");
		}

	}
	
	public ResponseData editDeposit(String data) {
		try {
			JSONObject obj = new JSONObject(data);
			
			Deposit deposit = repository.findById(obj.getLong("id")).get();
			deposit.setTermInMonths(obj.getInt("termInMonths"));
			
			repository.save(deposit);
			return new ResponseData(1, "Entity succesfully saved");
		}catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������������� �������� ����������� ��������!");
		}
	}
	
	public ResponseData createDeposit(String data) {
		try {
			JSONObject obj = new JSONObject(data);
			
			Deposit deposit = new Deposit();
			
			Bank bank = bankRepository.findById(obj.getString("bankBic")).get();
			Client client = clientRepository.findById(obj.getLong("clientId")).get();
			
			deposit.setBank(bank);
			deposit.setClient(client);
			deposit.setInterestRate(obj.getDouble("interestRate"));
			deposit.setOpeningDate(LocalDate.parse(obj.getString("openingDate")));
			deposit.setTermInMonths(obj.getInt("termInMonths"));
			
			repository.save(deposit);
			
			return new ResponseData(1, "Entity successfully created");
		}catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������� �������� ����������� ��������!");
		}
	}
	
	public ResponseData removeDeposit(String data) {
		try {
			JSONObject obj = new JSONObject(data);
			
			Deposit deposit = repository.findById(obj.getLong("id")).get();
			repository.delete(deposit);
			
			return new ResponseData(1, "Entity succesfully removed");			
		}catch (JSONException jEx) {
			return new ResponseData(-1, "���������� �������� �������� �����������!");
		} catch (Exception e) {
			return new ResponseData(-1, "�������� �������� ����������� ��������!");
		}
	}
	
	public ResponseData getDepositsOfClient(Long clientId) {
		try {
			List<Deposit> deposits = repository
					.findAllByClient(clientRepository
									.findById(clientId)
									.get());

			JSONObject resultObj = new JSONObject();
			JSONArray depositsArr = new JSONArray();
			
			for(Deposit d : deposits) {
			depositsArr.put(new JSONObject()
					.put("companyFullName", d.getClient().getFullCompanyName())
					.put("bankTitle", d.getBank().getTitle())
					.put("openingDate", d.getOpeningDate())
					.put("interestRate", d.getInterestRate())
					.put("termInMonths", d.getTermInMonths()));
			}
			resultObj.put("deposits", depositsArr);
			return new ResponseData(1, resultObj.toString());
		}catch (Exception e) {
			return new ResponseData(-1, "�� ����� ���������� ������ �������� ������! ���������� � ��������������!");
		}
	}
	
	public ResponseData getDepositsOfBank(String bankBic) {
		try {
			
			List<Deposit> deposits = repository
					.findAllByBank(bankRepository
								.findById(bankBic)
								.get());

			JSONObject resultObj = new JSONObject();
			JSONArray depositsArr = new JSONArray();
			
			for(Deposit d : deposits) {
			depositsArr.put(new JSONObject()
						.put("companyFullName", d.getClient().getFullCompanyName())
						.put("bankTitle", d.getBank().getTitle())
						.put("openingDate", d.getOpeningDate())
						.put("interestRate", d.getInterestRate())
						.put("termInMonths", d.getTermInMonths()));
			}
			resultObj.put("deposits", depositsArr);
			return new ResponseData(1, resultObj.toString());		
		}catch (Exception e) {
			return new ResponseData(-1, "�� ����� ���������� ������ �������� ������! ���������� � ��������������!");
		}
	}

}
