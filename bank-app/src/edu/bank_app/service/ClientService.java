package edu.bank_app.service;

import edu.bank_app.addons.ResponseData;

public interface ClientService {
	
	public ResponseData listAll();
	
	public ResponseData editClient(String data);
	
	public ResponseData removeClient(String data);
	
	public ResponseData addClient(String data);
	
	public ResponseData allSortedClients();
}
