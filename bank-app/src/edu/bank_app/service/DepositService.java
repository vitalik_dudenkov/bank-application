package edu.bank_app.service;

import edu.bank_app.addons.ResponseData;

public interface DepositService {
	
	public ResponseData listAll();
	
	public ResponseData editDeposit(String data);
	
	public ResponseData removeDeposit(String data);
	
	public ResponseData createDeposit(String data);
	
	public ResponseData getDepositsOfClient(Long clientId);
	
	public ResponseData getDepositsOfBank(String bankBic);
}
