package edu.bank_app.service;

import edu.bank_app.addons.ResponseData;

public interface BankService {
	
	public ResponseData listAll();
	
	public ResponseData editBank(String data);
	
	public ResponseData removeBank(String data);
	
	public ResponseData addBank(String data);
	
	public ResponseData searchBanks(String keyWord);
}
