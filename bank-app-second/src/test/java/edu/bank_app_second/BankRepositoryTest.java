package edu.bank_app_second;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import edu.bank_app_second.domain.Bank;
import edu.bank_app_second.domain.repository.BankRepository;

@RunWith(SpringRunner.class)
@EnableJpaRepositories(basePackages = {"edu_bank_app_second.domain.repositories"})
@Configuration
@EnableTransactionManagement
@SpringBootTest(classes = BankApplication.class)
public class BankRepositoryTest {
	
	@Autowired
	BankRepository repository;
	
	
	@Test
	public void whileCount_countFour() {
		assertTrue(repository.count() == 5L);
	}
	
	@Test
	public void testDeleteBank() {
		repository.deleteById("0412456612");
		assertNull(repository.findById("0412456612").orElse(null));
	}
	
	@Test
	public void testSaveBank() {
		Bank bank = new Bank();
		bank.setBic("0412456612");
		bank.setTitle("Тестовый банк");
		

		repository.save(bank);
		assertNotNull(bank.getBic());
		
		Bank fetchedBank = repository.findById("0412456612").orElse(null);
		
		assertNotNull(fetchedBank);
		
		assertEquals(bank.getBic(), fetchedBank.getBic());
		assertEquals(bank.getTitle(), fetchedBank.getTitle());
		
		fetchedBank.setTitle("Новое название");
		repository.save(fetchedBank);
		
		Bank fetchUpdatedBank = repository.findById(fetchedBank.getBic()).orElse(null);
		assertEquals(fetchedBank.getTitle(), fetchUpdatedBank.getTitle());
	}
}
