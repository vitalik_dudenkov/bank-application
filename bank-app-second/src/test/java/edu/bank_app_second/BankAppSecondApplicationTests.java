package edu.bank_app_second;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankApplication.class)
@WebAppConfiguration
class BankAppSecondApplicationTests {

	@Test
	void contextLoads() {
	}

}
