package edu.bank_app_second.domain;

import java.time.LocalDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "deposits")
public class Deposit{
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
    @ManyToOne
    @JoinColumn(name = "client_id")
	private Client client;
	
    @ManyToOne
    @JoinColumn(name = "bank_bic")
    private Bank bank;
    
    @Basic(optional = false)
    @Column(name = "opening_date")
    private LocalDate openingDate;
    
    @Basic(optional = false)
    @Column(name = "interest_rate")
    private double interestRate;
    
    @Basic(optional = false)
    @Column(name = "term_in_months")
    private int termInMonths;

	public long getId() {
		return id;
	}
    
	public void setId(long id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public LocalDate getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(LocalDate openingDate) {
		this.openingDate = openingDate;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getTermInMonths() {
		return termInMonths;
	}

	public void setTermInMonths(int termInMonths) {
		this.termInMonths = termInMonths;
	}
    

}
