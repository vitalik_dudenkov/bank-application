package edu.bank_app_second.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.bank_app_second.addons.LegalForm;


@Entity
@Table(name = "clients")
public class Client{
	
    @Id
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    
    @Basic(optional = false)
    @Column(name = "full_company_name")
    private String fullCompanyName;
    
    @Basic(optional = false)
    @Column(name = "short_company_name")
    private String shortCompanyName;
    
    @Basic(optional = false)
    @Column(name = "company_address")
    private String companyAddress;
    
    @Basic(optional = false)
    @Column(name = "company_legal_form")
    private LegalForm companyLegalForm;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public String getFullCompanyName() {
		return fullCompanyName;
	}

	public void setFullCompanyName(String fullCompanyName) {
		this.fullCompanyName = fullCompanyName;
	}

	public String getShortCompanyName() {
		return shortCompanyName;
	}

	public void setShortCompanyName(String shortCompanyName) {
		this.shortCompanyName = shortCompanyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public LegalForm getCompanyLegalForm() {
		return companyLegalForm;
	}

	public void setCompanyLegalForm(String val) {
		this.companyLegalForm = LegalForm.valueOf(val);
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", fullCompanyName=" + fullCompanyName + ", shortCompanyName=" + shortCompanyName
				+ ", companyAddress=" + companyAddress + ", companyLegalForm=" + companyLegalForm + "]";
	}

    
}
