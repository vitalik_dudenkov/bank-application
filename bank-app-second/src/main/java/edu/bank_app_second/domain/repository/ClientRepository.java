package edu.bank_app_second.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.bank_app_second.domain.Client;


public interface ClientRepository extends CrudRepository<Client, Long> {
	
	public List<Client> findAllByOrderByFullCompanyName();

}
