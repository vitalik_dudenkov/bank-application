package edu.bank_app_second.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.bank_app_second.domain.Bank;
import edu.bank_app_second.domain.Client;
import edu.bank_app_second.domain.Deposit;


public interface DepositRepository extends CrudRepository<Deposit, Long>{
	
	List<Deposit> findAllByClient(Client client);
	
	List<Deposit> findAllByBank(Bank bank);
}
