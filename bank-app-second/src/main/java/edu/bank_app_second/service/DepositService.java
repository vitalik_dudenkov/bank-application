package edu.bank_app_second.service;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.resource.DepositResource;

public interface DepositService {
	
	public ResponseData listAll();
	
	public ResponseData editDeposit(DepositResource depositResource);
	
	public ResponseData removeDeposit(Long id);
	
	public ResponseData createDeposit(DepositResource depositResource);
	
	public ResponseData getDepositsOfClient(Long clientId);
	
	public ResponseData getDepositsOfBank(String bankBic);
}
