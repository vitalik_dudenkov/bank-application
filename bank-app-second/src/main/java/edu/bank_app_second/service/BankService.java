package edu.bank_app_second.service;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.resource.BankResource;

public interface BankService {
	
	public ResponseData listAll();
	
	public ResponseData editBank(BankResource bankResource);
	
	public ResponseData removeBank(String bic);
	
	public ResponseData addBank(BankResource bankResource);
	
	public ResponseData searchBanks(String keyWord);
}
