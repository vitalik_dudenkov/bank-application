package edu.bank_app_second.service;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.resource.ClientResource;

public interface ClientService {
	
	public ResponseData listAll();
	
	public ResponseData editClient(ClientResource clientResource);
	
	public ResponseData removeClient(Long id);
	
	public ResponseData addClient(ClientResource clientResource);
	
	public ResponseData allSortedClients();
}
