package edu.bank_app_second.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.domain.Bank;
import edu.bank_app_second.domain.Client;
import edu.bank_app_second.domain.Deposit;
import edu.bank_app_second.domain.repository.BankRepository;
import edu.bank_app_second.domain.repository.ClientRepository;
import edu.bank_app_second.domain.repository.DepositRepository;
import edu.bank_app_second.resource.DepositResource;


@Service("DepositService")
@Transactional
public class DepositServiceImpl implements DepositService {

	@Autowired
	private DepositRepository repository;
	
	@Autowired
	private BankRepository bankRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Override
	public ResponseData listAll(){
		
		List<String> depositsJson = new ArrayList<>();
		try {
			Iterable<Deposit> deposits = repository.findAll();
			for(Deposit d : deposits) {
				depositsJson.add(new DepositResource(d.getId(),
									d.getClient().getFullCompanyName(),
									d.getBank().getTitle(), d.getOpeningDate(),
									d.getInterestRate(), d.getTermInMonths()).toJson());
			}
			return new ResponseData(1, depositsJson.toString());
		}catch (Exception e) {
			return new ResponseData(-1, "Ошибка извлечения данных! Обратитесь к администратору!");
		}

	}
	
	// Принято решение, что у вклада кроме длительности менять ничего нельзя
	@Override
	public ResponseData editDeposit(DepositResource depositResource) {
		try {
			Deposit deposit = repository.findById(depositResource.getId()).get();
			deposit.setTermInMonths(depositResource.getTermInMonths());
			
			repository.save(deposit);
			return new ResponseData(1, "Сущность успешно отредактирована!");
		} catch (Exception e) {
			return new ResponseData(-1, "Во время сохранения возникла ошибка! Обратитесь к администратору!");
		}
	}
	
	@Override
	public ResponseData createDeposit(DepositResource depositResource) {
		try {
			
			Deposit deposit = new Deposit();
			
			Bank bank = bankRepository.findById(depositResource.getBankBic()).get();
			Client client = clientRepository.findById(depositResource.getClientId()).get();
			
			deposit.setBank(bank);
			deposit.setClient(client);
			deposit.setInterestRate(depositResource.getInterestRate());
			deposit.setOpeningDate(depositResource.getOpeningDate());
			deposit.setTermInMonths(depositResource.getTermInMonths());
			
			repository.save(deposit);
			
			return new ResponseData(1, "Сущность успешно создана!");
		}catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка во время создания сущности!");
		}
	}
	
	@Override
	public ResponseData removeDeposit(Long id) {
		try {
			Deposit deposit = repository.findById(id).get();
			repository.delete(deposit);
			
			return new ResponseData(1, "Сущность удалена!");			
		}catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка во время удаления!");
		}
	}
	
	@Override
	public ResponseData getDepositsOfClient(Long clientId) {
		try {
			List<Deposit> deposits = repository
					.findAllByClient(clientRepository
									.findById(clientId)
									.get());

			List<String> depositsJson = new ArrayList<>();
			
			for(Deposit d : deposits) {
			depositsJson.add(new DepositResource(d.getId(),
							d.getClient().getFullCompanyName(),
							d.getBank().getTitle(), d.getOpeningDate(),
							d.getInterestRate(), d.getTermInMonths()).toJson());
			}
			return new ResponseData(1, depositsJson.toString());
		}catch (Exception e) {
			return new ResponseData(-1, "Ошибка извлечения данных! Обратитесь к администратору!");
		}
	}
	
	@Override
	public ResponseData getDepositsOfBank(String bankBic) {
		try {
			
			List<Deposit> deposits = repository
					.findAllByBank(bankRepository
								.findById(bankBic)
								.get());

			List<String> depositsJson = new ArrayList<>();
			
			for(Deposit d : deposits) {
			depositsJson.add(new DepositResource(d.getId(),
							d.getClient().getFullCompanyName(),
							d.getBank().getTitle(), d.getOpeningDate(),
							d.getInterestRate(), d.getTermInMonths()).toJson());
			}
			return new ResponseData(1, depositsJson.toString());		
		}catch (Exception e) {
			return new ResponseData(-1, "Ошибка извлечения данных! Обратитесь к администратору!");
		}
	}

}
