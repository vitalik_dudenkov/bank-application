package edu.bank_app_second.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.domain.Bank;
import edu.bank_app_second.domain.repository.BankRepository;
import edu.bank_app_second.resource.BankResource;


@Service("BankService")
@Transactional
public class BankServiceImpl implements BankService {
	
	@Autowired
	private BankRepository repository;
	
	@Override
	public ResponseData listAll(){		
		List<String> banksJson = new ArrayList<>();
		try {
			Iterable<Bank> banks = repository.findAll();
			for(Bank b : banks) {
				banksJson.add(new BankResource(b.getBic(), b.getTitle()).toJson());
			}
			return new ResponseData(1, banksJson.toString());
		}catch (Exception e) {
			return new ResponseData(-1, "Ошибка извлечения данных! Обратитесь к администратору!");
		}

	}
	
	@Override
	public ResponseData editBank(BankResource bankResource) {
		try {
			Bank bank = repository.findById(bankResource.getBic()).get();
			bank.setTitle(bankResource.getTitle());
			repository.save(bank);
			return new ResponseData(1, "Сущность сохранена!");
		} catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка сохранения!");
		}
	}
	
	@Override
	public ResponseData removeBank(String bic) {
		try {
			Bank bank = repository.findById(bic).get();
			repository.delete(bank);
			return new ResponseData(1, "Сущность удалена!");
		} catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка удаления!");
		}
	}
	
	@Override
	public ResponseData addBank(BankResource bankResource) {
		try {
			Bank bank = new Bank();
			bank.setBic(bankResource.getBic());
			bank.setTitle(bankResource.getTitle());
			
			repository.save(bank);
			return new ResponseData(1, "Сущность успешно создана!");
		} catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка при создании сущности!");
		}

	}
	
	@Override
	public ResponseData searchBanks(String keyWord) {		
		List<String> banksJson = new ArrayList<>();

		try {
			List<Bank> banks = repository.search(keyWord);
			
			for(Bank b : banks) {
				banksJson.add(new BankResource(b.getBic(), b.getTitle()).toJson());
			}
			return new ResponseData(1, banksJson.toString());
		}catch (Exception e) {
			return new ResponseData(-1, "Ошибка извлечения данных! Обратитесь к администратору!");
		}
	}

}
