package edu.bank_app_second.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.domain.Client;
import edu.bank_app_second.domain.repository.ClientRepository;
import edu.bank_app_second.resource.ClientResource;


@Service("ClientService")
@Transactional
public class ClientServiceImpl implements ClientService {
	
	@Autowired
	private ClientRepository repository;
	
	@Override
	public ResponseData listAll(){
		List<String> clientsJson = new ArrayList<String>();
		try {
			Iterable<Client> clients = repository.findAll();
			for(Client c : clients) {
				clientsJson.add(new ClientResource(c.getId(), c.getFullCompanyName(),
						c.getShortCompanyName(), c.getCompanyAddress(),
						c.getCompanyLegalForm()).toJson());
			}
			return new ResponseData(1, clientsJson.toString());
		} catch (Exception e) {
			return new ResponseData(-1, "Ошибка извлечения данных! Обратитесь к администратору!");
		}

	}
	
	@Override
	public ResponseData editClient(ClientResource clientResource) {
		try {
			
			Client client = repository.findById(clientResource.getId()).get();
			client.setFullCompanyName(clientResource.getFullName());
			client.setShortCompanyName(clientResource.getShortName());
			client.setCompanyAddress(clientResource.getAddress());
			client.setCompanyLegalForm(clientResource.getLegalForm().name());
			
			repository.save(client);
			return new ResponseData(1, "Сущность сохранена!");			
		} catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка во время сохранения сущности!");
		}

	}
	
	@Override
	public ResponseData removeClient(Long id) {
		try {
			Client client = repository.findById(id).get();
			repository.delete(client);
			return new ResponseData(1, "Сущность удалена!");			
		} catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка во время удаления!");
		}

	}
	
	@Override
	public ResponseData addClient(ClientResource clientResource) {
		try {			
			Client client = new Client();
			client.setFullCompanyName(clientResource.getFullName());
			client.setShortCompanyName(clientResource.getShortName());
			client.setCompanyAddress(clientResource.getAddress());
			client.setCompanyLegalForm(clientResource.getLegalForm().name());
			
			repository.save(client);
			return new ResponseData(1, "Сущность успешно создана!");			
		} catch (Exception e) {
			return new ResponseData(-1, "Произошла ошибка во время создания сущности!");
		}
	}
	
	@Override
	public ResponseData allSortedClients() {
		List<String> clientsJson = new ArrayList<String>();		
		
		try {
			List<Client> clients = repository.findAllByOrderByFullCompanyName();
			for(Client c : clients) {
				clientsJson.add(new ClientResource(c.getId(), c.getFullCompanyName(),
						c.getShortCompanyName(), c.getCompanyAddress(),
						c.getCompanyLegalForm()).toJson());
			}
			return new ResponseData(1, clientsJson.toString());			
		}catch (Exception e) {
			return new ResponseData(-1, "Ошибка извлечения данных! Обратитесь к администратору!");
		}
	}
}
