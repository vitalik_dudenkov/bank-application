package edu.bank_app_second.addons;


/**
 * Класс <code>ResponseData</code> помогает сообщить пользоватю понятно о возникших в программе исключениях.
 * Используется во всех сервисах в качестве возвращаемого значения
 * 
 * @author DudenkovVI
 *
 */

public class ResponseData {
	private int errorCode;
	private String data;
	
	public ResponseData() {
		super();
	}

	public ResponseData(int errorCode, String data) {
		super();
		this.errorCode = errorCode;
		this.data = data;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	
	
}
