package edu.bank_app_second.addons;

public enum LegalForm {
	SP,
	FARM,
	JSC,
	LLC,
	PJSC,
	NJSC,
	GP,
	LP,
	EP;
}
