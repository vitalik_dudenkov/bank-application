package edu.bank_app_second.resource;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * Класс <code>BankResource</code> является POJO и может быть использован так же для упаковки
 * 
 * ответа сервера клиенту.
 * 
 * @author DudenkovVI
 *
 */

public class BankResource implements Serializable {

    private ObjectMapper mapper = new ObjectMapper();
    private static final long serialVersionUID = 1L;

    private String bic;
    private String title;
    
    
    private void initMapper() {
        mapper.findAndRegisterModules();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new ToStringSerializer());
        mapper.registerModule(module);
    }
    
	public BankResource() {
		super();
		initMapper();
	}
	
	public BankResource(String bic, String title) {
		super();
		initMapper();
		this.bic = bic;
		this.title = title;
	}
	
	public String getBic() {
		return bic;
	}
	public void setBic(String bic) {
		this.bic = bic;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
    
    public String toJson() {
        String json;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException ignore) {
            json = "{\"code\" : \"-100\"}";
        }
        return json;
    }
}
