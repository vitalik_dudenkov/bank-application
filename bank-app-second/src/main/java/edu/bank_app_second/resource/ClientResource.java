package edu.bank_app_second.resource;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import edu.bank_app_second.addons.LegalForm;

public class ClientResource implements Serializable {
    private ObjectMapper mapper = new ObjectMapper();
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String fullName;
    private String shortName;
    private String address;
    private LegalForm legalForm;
    
    private void initMapper() {
        mapper.findAndRegisterModules();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new ToStringSerializer());
        mapper.registerModule(module);
    }
     

	public ClientResource() {
		super();
		initMapper();
	}
	
	

	public ClientResource(Long id, String fullName, String shortName, String address, LegalForm legalForm) {
		super();
		initMapper();
		this.id = id;
		this.fullName = fullName;
		this.shortName = shortName;
		this.address = address;
		this.legalForm = legalForm;
	}


	public ClientResource(String fullName, String shortName, String address, LegalForm legalForm) {
		super();
		initMapper();
		this.fullName = fullName;
		this.shortName = shortName;
		this.address = address;
		this.legalForm = legalForm;
	}
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LegalForm getLegalForm() {
		return legalForm;
	}

	public void setLegalForm(LegalForm legalForm) {
		this.legalForm = legalForm;
	}
        
    public String toJson() {
        String json;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException ignore) {
            json = "{\"code\" : \"-100\"}";
        }
        return json;
    }

}
