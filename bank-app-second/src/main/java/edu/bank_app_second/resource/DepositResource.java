package edu.bank_app_second.resource;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class DepositResource implements Serializable{

    private ObjectMapper mapper = new ObjectMapper();
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private Long clientId;
    private String clientName;
    private String bankBic;
    private String bankTitle;
    private LocalDate openingDate;
    private Double interestRate;
    private Integer termInMonths;
    
    
	public DepositResource() {
		super();
		initMapper();
	}
	

	public DepositResource(Long id, Integer termInMonths) {
		super();
		initMapper();
		this.id = id;
		this.termInMonths = termInMonths;
	}
	

	public DepositResource(Long clientId, String bankBic, LocalDate openingDate, Double interestRate,
			Integer termInMonths) {
		super();
		initMapper();
		this.clientId = clientId;
		this.bankBic = bankBic;
		this.openingDate = openingDate;
		this.interestRate = interestRate;
		this.termInMonths = termInMonths;
	}


	public DepositResource(String clientId, String bankTitle, LocalDate openingDate, Double interestRate,
			Integer termInMonths) {
		super();
		initMapper();
		this.clientName = clientId;
		this.bankTitle = bankTitle;
		this.openingDate = openingDate;
		this.interestRate = interestRate;
		this.termInMonths = termInMonths;
	}

	public DepositResource(Long id, String clientName, String bankTitle, LocalDate openingDate, Double interestRate,
			Integer termInMonths) {
		super();
		initMapper();
		this.id = id;
		this.clientName = clientName;
		this.bankTitle = bankTitle;
		this.openingDate = openingDate;
		this.interestRate = interestRate;
		this.termInMonths = termInMonths;
	}
	
	private void initMapper() {
        mapper.findAndRegisterModules();
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new ToStringSerializer());
        mapper.registerModule(module);
	}
	

	public Long getClientId() {
		return clientId;
	}


	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}


	public String getBankBic() {
		return bankBic;
	}


	public void setBankBic(String bankBic) {
		this.bankBic = bankBic;
	}


	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getClientName() {
		return clientName;
	}
	
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	public String getBankTitle() {
		return bankTitle;
	}
	
	public void setBankTitle(String bankTitle) {
		this.bankTitle = bankTitle;
	}
	
	public LocalDate getOpeningDate() {
		return openingDate;
	}
	
	public void setOpeningDate(LocalDate openingDate) {
		this.openingDate = openingDate;
	}
	
	public Double getInterestRate() {
		return interestRate;
	}
	
	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}
	
	public Integer getTermInMonths() {
		return termInMonths;
	}
	
	public void setTermInMonths(Integer termInMonths) {
		this.termInMonths = termInMonths;
	}
    
	
    public String toJson() {
        String json;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException ignore) {
            json = "{\"code\" : \"-100\"}";
        }
        return json;
    }
    
}
