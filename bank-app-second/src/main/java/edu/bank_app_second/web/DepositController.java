package edu.bank_app_second.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.resource.DepositResource;
import edu.bank_app_second.service.DepositService;



/**
 * Класс <code>DepositController</code> отвечает на запросы, связанные с сущностью Deposit
 * 
 * @author DudenkovVI
 *
 */

@RestController
@RequestMapping("/deposit")
public class DepositController {

	@Autowired
	private DepositService service;
	
	@GetMapping("/allDeposits")
	public ResponseEntity<String> allDeposits(){
		ResponseData data = service.listAll();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/editDeposit", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> editDeposit(@RequestBody DepositResource depositResource){
		ResponseData data = service.editDeposit(depositResource);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/createDeposit", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createDeposit(@RequestBody DepositResource depositResource){
		ResponseData data = service.createDeposit(depositResource);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/removeDeposit", consumes = "multipart/form-data")
	public ResponseEntity<String> removeDeposit(@RequestParam(value = "id") Long id){
		ResponseData data = service.removeDeposit(id);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("/getDepositsOfClient")
	public ResponseEntity<String> getDepositsOfClient(@RequestParam("clientId") String clientId){
		ResponseData data;
		try {
			data = service.getDepositsOfClient(Long.parseLong(clientId));
		}catch (Exception e) {
			data = new ResponseData(-1, "Переданный идентификатор не число!");
		}
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("/getDepositsOfBank")
	public ResponseEntity<String> getDepositsOfBank(@RequestParam("bankBic") String bankBic){
		ResponseData data = service.getDepositsOfBank(bankBic);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
}
