package edu.bank_app_second.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.resource.ClientResource;
import edu.bank_app_second.service.ClientService;

/**
 * Класс <code>ClientController</code> отвечает на запросы, связанные с сущностью Client
 * 
 * @author DudenkovVI
 *
 */

@RestController
@RequestMapping("/client")
public class ClientController {
	
	@Autowired
	private ClientService service;
	
	@GetMapping("/allClients")
	public ResponseEntity<String> allClients(){
		ResponseData data = service.listAll();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/editClient", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> editClient(@RequestBody ClientResource clientResource){
		ResponseData data = service.editClient(clientResource);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);			
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/removeClient", consumes = "multipart/form-data")
	public ResponseEntity<String> removeClient(@RequestParam(value="id") Long id){
		ResponseData data = service.removeClient(id);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);		
//		return new ResponseEntity<String>(data.getData(), data.getErrorCode() > 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);

	}
	
	@PostMapping(value = "/addClient", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addClient(@RequestBody ClientResource clientResource){
		ResponseData data = service.addClient(clientResource);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);				
	}
	
	@GetMapping("/sortedClients")
	public ResponseEntity<String> sortedClients(){
		ResponseData data = service.allSortedClients();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);						
	}
	
}
