package edu.bank_app_second.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.bank_app_second.addons.ResponseData;
import edu.bank_app_second.resource.BankResource;
import edu.bank_app_second.service.BankService;


/**
 * Класс <code>BankController</code> отвечает на запросы, связанные с сущностью Bank
 * 
 * @author DudenkovVI
 *
 */

@RestController
@RequestMapping("/bank")
public class BankController {

	@Autowired
	private BankService service;
	
	@GetMapping("/allBanks")
	public ResponseEntity<String> allBanks(){
		ResponseData data = service.listAll();
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);			
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/editBank", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> editBank(@RequestBody BankResource bankResource){
		ResponseData data = service.editBank(bankResource);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/removeBank", consumes = "multipart/form-data")
	public ResponseEntity<String> removeBank(@RequestParam(value = "bic") String bic){
		ResponseData data = service.removeBank(bic);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping(value = "/addBank", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addBank(@RequestBody BankResource bankResource){
		ResponseData data = service.addBank(bankResource);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(value = "/search")
	public ResponseEntity<String> search(@RequestParam(value = "keyWord") String keyWord) {
		ResponseData data = service.searchBanks(keyWord);
		if(data.getErrorCode() > 0) {
			return new ResponseEntity<String>(data.getData(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(data.getData(), HttpStatus.BAD_REQUEST);
	}
	
}
